<section class="w-100 d-flex backgroundDarkGray align-items-center overflow-hidden justify-content-center infoNav">
    <article class="d-flex flex-row maxWidth col-12 p-2">
        <p class="align-items-center m-0 colorWhiteFont col-sm-6 p-0 col-md-4 col-lg-3">
            <i class="fas fa-phone-alt colorBlueFont pr-2"></i>
            <?php echo $this->getContactInfo()->getPhone(); ?>
        </p>
        <p class="align-items-center m-0 colorWhiteFont p-0 col-sm-5 col-md-6 col-lg-8">
            <i class="fas fa-envelope colorBlueFont pr-2"></i>
            <?php echo $this->getContactInfo()->getEmail(); ?>
        </p>

        <?php foreach ($this->getIcons() as $name => $url): ?>
            <a class="align-self-end justify-content-end col-sm-1 d-flex col-lg-1 p-0" target="_blank" href="<?php echo $url; ?>"><?php echo $name; ?></a>
        <?php endforeach; ?>

    </article>

</section>