<div class="bg-white mainNavDiv d-flex align-items-center justify-content-center">
    <nav class="navbar navbar-expand-lg bg-white maxWidth p-0 d-flex col-12">
        <a class="navbar-brand  col-4 " href="../../index.php">
            <?php if($this->getLogoText() != "") : ?>
                <?php echo $this->getLogoText(); ?>
            <?php else: ?>
                <img src="<?php echo $this->getLogoImage(); ?>" class="logo" alt="">
            <?php endif; ?>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseNav">

            <i class="fas fa-bars fa-2x colorLightBlueFont"></i>

        </button>

        <div class="collapse navbar-collapse bg-white" id="collapseNav">

            <ul class="navbar-nav ml-auto d-flex align-items-center justify-content-center">

                <?php foreach($this->getOptions() as $key => $value): ?>

                    <li class="nav-item dropdown">

<!--                        --><?php //if(is_array($value)) : ?>
<!--                            <a class="nav-link colorGreyFont dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" href="../../index.php">--><?php //echo $key; ?><!--</a>-->
<!--                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">-->
<!---->
<!--                                --><?php //foreach($value as $item): ?>
<!--                                    <a class="dropdown-item">--><?php //echo $item; ?><!--</a>-->
<!--                                --><?php //endforeach; ?>
<!---->
<!--                            </div>-->
<!---->
<!--                        --><?php //else: ?>

                        <?php if($key) : ?>
                            <a class="nav-link colorGreyFont" href="<?php echo strtolower($value); ?>.php"><?php echo $key; ?></a>
                        <?php else :?>
                            <a class="nav-link colorGreyFont" href="<?php echo strtolower($value); ?>.php"><?php echo $value; ?></a>
                        <?php endif; ?>
                    </li>

                <?php endforeach; ?>

            </ul>
        </div>
    </nav>
</div>