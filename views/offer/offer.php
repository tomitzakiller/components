<div class=" d-flex backgroundDarkerGray h-auto w-100 align-items-center justify-content-center">
    <section class="maxWidth d-flex flex-wrap flex-column">
        <h2 class="colorLightBlueFont font-weight-bold header mt-5 mb-3 pl-2">NAŠA
            <span class=" header colorDarkGreyFont">PONUDA</span>
        </h2>

        <article class="d-flex flex-wrap mb-5 p-0 justify-content-center col-12 align-self-center">
            <?php foreach ($this->getOffer() as $key => $value): ?>
            <a href="o-nama.php" class="btn btn-default button bg-white colorDarkGreyFont  buttonTextShadow buttonOffer">
                <?php echo $value." ".$key; ?>
            </a>
            <?php endforeach; ?>
        </article>
    </section>

</div>