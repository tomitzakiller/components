<div class="d-flex  justify-content-center align-items-center backgroundLighterGray pt-5" >

    <section class="maxWidth d-flex flex-row flex-wrap align-items-center justify-content-center id="contact"">
        <article class="col-12 col-lg-6 d-flex flex-column">
            <h2 class="mb-3 font-weight-bold colorDarkGreyFont header">POŠALJITE
               <span class="colorLightBlueFont header">PORUKU</span>
            </h2>
            <form class="form-group col-12 col-md-6 col-lg-8 p-2" method="POST" action="sendmail.php">

                <input type="text" name="name" class="checkInputName form-control p-3 mt-3 rounded-0" aria-describedby="emailHelp" placeholder="Vaše ime i prezime" required="">

                <input type="email" name="email" class="checkInputEmail form-control p-3 mt-3 rounded-0" aria-describedby="emailHelp" placeholder="Vaša e-mail adresa" required="">
                <input type="text" name="phone" class="checkInputEmail form-control p-3 mt-3 rounded-0" aria-describedby="emailHelp" placeholder="Kontakt telefon" required="">

                <textarea name="message" class="checkInputText form-control p-3 mt-3 rounded-0 noResize" id="exampleFormControlTextarea1" rows="5" placeholder="Poruka" required="">

                </textarea>

                <input type="text" id="website" name="msg"/>

                <button name="submit" class="btn btn-default button backgroundYellow colorWhiteFont font-weight-bold mt-3 mb-5">POŠALJI</button>
            </form>
        </article>

        <article class="d-flex flex-column col-12 col-lg-6 justify-content-center align-self-start">

            <h2 class="mb-3 font-weight-bold colorDarkGreyFont">KONTAKTIRAJTE NAS</h2>
            <p class="colorGreyFont mt-3 colorGreyFont"><?php echo $this->getTitle(); ?></p>
            <p class="colorGreyFont colorGreyFont">
                <?php echo $this->getText(); ?>
            </p>

            <div class="w-100 col-12 d-flex flex-row flex-wrap">

                <div class="col-12 col-md-6 col-lg-6 d-flex flex-column justify-content-start pt-sm-3">
                    <i class="fas fa-map-marked-alt display-4 p-2 colorLightBlueFont"></i>
                    <h4 class="p-1 colorDarkGreyFont">Adresa</h4>
                    <p class="colorGreyFont m-0 "><?php echo $this->getAddress(); ?></p>
                    <p class="colorGreyFont m-0 "><?php echo $this->getCity(); ?></p>
                </div>

                <div class="col-12 col-md-6 col-lg-6 d-flex flex-column justify-content-start pt-sm-3">
                    <i class="fas fa-mail-bulk display-4 p-2 colorLightBlueFont"></i>
                    <h4 class=" p-1 colorDarkGreyFont">Kontakt</h4>
                    <p class="colorGreyFont m-0"><?php echo $this->getEmail(); ?></p>
                    <p class="colorGreyFont m-0"><?php echo $this->getPhone(); ?></p>
                </div>

            </div>

        </article>

    </section>

</div>

<section class="w-100 d-flex">

    <iframe class="w-100" src="<?php echo $this->getUrl(); ?>" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

</section>