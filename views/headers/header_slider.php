<style>
    .carousel-item {
        height: 100vh;
        min-height: 350px;
        background: no-repeat center center scroll;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>

<header>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

        <div class="position-absolute w-100 h-100">
            <section class="h-100 headerName paddingLeft w-100 col-12 d-flex justify-content-center flex-column" style="z-index: 500">
                <h1 class="colorWhiteFont headerFont pl-2">ADAPTACIJA &nbsp;
                    <span class="colorYellowFont headerFont"> STANOVA</span>
                </h1>
                <article class="d-flex flex-row align-items-center headerButtons flex-wrap pt-2 pl-2">
                    <a href="kontakt.php" class="btn btn-default  button backgroundTransparent colorWhiteFont mr-4 buttonBorder buttonContact">KONTAKT</a>
                    <a href="galerija.php" class="btn btn-default button backgroundYellow colorWhiteFont ml-4 buttonTextShadow buttonGallery">GALERIJA</a>
                </article>
            </section>
        </div>


        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <?php $counter = 0; ?>
            <?php foreach($this->getImages() as $image) : ?>
                <div class="carousel-item <?php echo $counter == 0 ? "active" : ""; ?>" style="background-image: url('<?php echo $image; ?>')"></div>
                <?php $counter++; ?>
            <?php endforeach; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>