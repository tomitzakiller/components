<div class="d-flex flex-wrap w-100 align-items-center justify-content-center h-auto backgroundLightGrey">
    <section class="d-flex flex-wrap maxWidth">
        <h2 class="colorLightBlueFont header  pt-5 font-weight-bold pl-2">ADAPTACIJA
            <span class="header colorDarkGreyFont">&nbsp;STANOVA</span>
        </h2>
        <article class="d-flex flex-column align-items-center justify-content-center col-12">
            <p class="colorGreyFont adaptionText p-lg-1 text-justify">
                <span class="ml-5">Naš</span>
                tim ljudi sposoban je da vašu maštu pretvori u stvarnost, i pomoći vam da živite vaše snove. Od vašeg običnog životnog prostora napravićemo jedinstveno i originalno mesto kakvo ste oduvež želeli. Ukoliko niste sigurni šta tačno želite, ili kakav prostor bi vam najviše odgovarao, naš arhitekta enterijera će vas posavetovati ili vam isprojektovati prostor preva vašem senzibilitetu i ličnosti. Životni prostor nije samo kutija sa otvorima, niti se on svodi na odabiranje nameštaja u salonu. On može i mora biti mnogo više od toga, a mi smo tu da vam to i dokažemo.
            </p>
            <img class="w-100 h-auto" alt="image" src="images/before_and_after.png" />
        </article>
        <a href="o-nama.php" class="btn btn-default button backgroundYellow colorWhiteFont ml-4 font-weight-bold buttonGallery mt-3 mb-5">SAZNAJ VIŠE</a>
    </section>

</div>