<div class="d-flex flex-wrap w-100  h-auto backgroundLightGrey justify-content-center align-items-center">
    <section class="d-flex flex-wrap maxWidth">
        <h2 class="colorYellowFont header pt-5  font-weight-bold pl-2">ADAPTACIJA
            <span class="header colorDarkGreyFont">&nbsp;POSLOVNOG PROSTORA</span>
        </h2>
        <article class="d-flex flex-column align-items-center justify-content-center col-12 ">
            <p class="colorGreyFont adaptionText text-justify p-lg-1">
                <span class="ml-5">Poslovni </span>
                prostori su vaš drugi dom, mesto u kome provodite veliku količinu vremena, neretno i veću nego u privatnom prostoru i on zaslužuje da o njegovom izgledu i kvalitetu brinu i pravi stručnjaci. Renoviranje, adaptacija ili projektovanje vaših poslovnih protora u rukama našeg tima za rezultat uvek ima jedinstven doživljaj koji dajemo vašim svakodnevnim obavezama koji u našim rukama postaju kvalitetno i lično vreme u kome ćete ostvarivati vaše najbolje rezultate.
            </p>
            <img class="w-100 h-auto" alt="image" src="images/business.png"" />
        </article>
        <a href="o-nama.php" class="btn btn-default button backgroundBlue colorWhiteFont ml-4 font-weight-bold buttonGallery mt-3 mb-5">SAZNAJ VIŠE</a>
    </section>
</div>