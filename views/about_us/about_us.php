<div class="d-flex flex-wrap w-100 h-auto backgroundLightGrey justify-content-center align-items-center">
    <section class="d-flex maxWidth flex-wrap h-auto">
        <h2 class="colorLightBlueFont header  pt-5 font-weight-bold">
            <span class="header colorDarkGreyFont">&nbsp;<?php echo $this->getHeader(); ?></span>
        </h2>
        <article class="d-flex flex-column align-items-center justify-content-center col-12 pb-4 h-auto">
            <p class="colorGreyFont adaptionText p-lg-1 text-justify">
                <?php echo $this->getText();?>
            </p>

            <div class="d-flex flex-wrap flex-row align-items-center justify-content-center h-auto">
                <img class="col-sm-11 col-md-6 col-lg-6 h-auto p-2" alt="image" src="<?php echo $this->getImage(); ?>" />
            </div>

        </article>

        <aside class="d-flex flex-wrap justify-content-center mb-4">
            <?php foreach($this->offerData as $key => $value) : ?>

                <div class="card col-sm-11 col-md-5 col-lg-5 m-2 d-flex justify-content-center">
                    <div class="card-body">
                        <h5 class="card-title colorDarkGreyFont"><?php echo $value['icon']."&nbsp;&nbsp;".$key; ?></h5>
                        <ul class="list-unstyled colorGreyFont">
                            <?php foreach ($value as $k => $val) : ?>
                                <?php if($k !== "icon") : ?>
                                    <li>- <?php echo $val; ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

            <?php endforeach; ?>
        </aside>

    </section>
</div>