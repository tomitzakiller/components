<section class="d-flex flex-wrap flex-column col-12 backgroundBlue align-items-center text-center">

    <h3 class="colorWhiteFont font-weight-bold pt-5 pb-2">IMATE PITANJA?</h3>
    <h3 class="colorWhiteFont font-weight-bold  pb-lg-5 pb-md-2 pb-sm-2">SPREMNI STE DA NAPRAVITE PRVI KORAK?</h3>

    <article class="d-flex flex-wrap align-items-center mb-5 justify-content-center">

        <a onclick="FB.CustomerChat.show();" class="btn btn-default button bg-white colorLightBlueFont font-weight-bold buttonGallery mr-5 text-center">LIVE CHAT</a>

        <a href="kontakt.php" class="btn btn-default button colorWhiteFont backgroundYellow font-weight-bold buttonGallery text-center">ZATRAZI PROCENU</a>

    </article>

</section>