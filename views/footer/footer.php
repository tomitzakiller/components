<div class="d-flex col-12  backgroundDarkGray p-0 justify-content-center">

    <footer class="maxWidth d-flex flex-row flex-wrap align-items-center justify-content-center">

        <article class="col-12 d-flex flex-row flex-wrap align-items-center justify-content-center pt-3">
            <div class="col-12  col-md-4 col-lg-4 d-flex flex-column align-items-center justify-content-center pt-4">
                <i class="fas fa-map-marker-alt colorWhiteFont footerIcons"></i>
                <p class="colorGreyFont m-0 colorWhiteFont pt-2 ">
                    <?php echo  $this->getContactInfo()->getAddress(); ?> <br>
                    <?php echo  $this->getContactInfo()->getCity(); ?>
                </p>
            </div>

            <div class="col-12 col-md-4 col-lg-4 d-flex flex-column align-items-center justify-content-center pt-4">
                <i class="fas fa-envelope colorWhiteFont footerIcons"></i>
                <p class="colorGreyFont m-0 colorWhiteFont pt-2">
                    <?php echo  $this->getContactInfo()->getEmail(); ?>
                </p>
            </div>

            <div class="col-12 col-md-4 col-lg-4 d-flex flex-column align-items-center justify-content-center pt-4">
                <i class="fas fa-phone-alt colorWhiteFont footerIcons"></i>
                <p class="colorGreyFont m-0 colorWhiteFont pt-2">
                    <?php echo  $this->getContactInfo()->getPhone(); ?>
                </p>
            </div>
        </article>

        <article class=" col-12 mt-3">
            <?php if ($this->getYear() == date('Y')) : ?>
                <p class="m-3 colorWhiteFont text-center">© <?php echo date('Y'); ?> All Rights Reserved</p>
            <?php else : ?>
                <p class="m-3 colorWhiteFont text-center">© <?php echo $this->getYear(); ?> - <?php echo date('Y'); ?> All Rights Reserved</p>
            <?php endif; ?>
            <p class="m-3 colorWhiteFont text-center">Designed & created by <a target="_blank" href="https://jelenazivanovic.com">Jelena Živanović</a></p>
        </article>
    </footer>

</div>