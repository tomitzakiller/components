<section class="d-flex flex-wrap col-12 align-items-center justify-content-center p-0 backgroundLightGrey">
    <article class="maxWidth d-flex flex-wrap pb-4 pt-4">
        <?php foreach($this->getImages() as $image) : ?>
            <img class=" h-auto p-2 p-sm-3 col-sm-12 col-lg-6 " src="<?php echo $image; ?>" alt="images">
        <?php endforeach; ?>
    </article>
</section>