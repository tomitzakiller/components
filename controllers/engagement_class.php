<?php


class EngagementClass
{

    /**
     * @var string
     */
    private $type = "";

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function create()
    {
        switch($this->type)
        {
            case "chat":
                require_once __DIR__."/../views/chat/chat.php";
                break;

            default:
                break;
        }
    }

}