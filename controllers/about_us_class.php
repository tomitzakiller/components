<?php

    class AboutUsClass
    {
        /**
         * @var string
         */
        private $header = "";


        /**
         * @var string
         */
        private $text = "";

        /**
         * @var string
         */
        private $image = "";


        /**
         * @var array
         */
        private $offerData = [];

        /**
         * @return array
         */
        public function getOfferData()
        {
            return $this->offerData;
        }

        /**
         * @param array $offerData
         */
        public function setOfferData($offerData)
        {
            $this->offerData = $offerData;
        }

        /**
         * @return string
         */
        public function getHeader()
        {
            return $this->header;
        }

        /**
         * @param string $header
         */
        public function setHeader($header)
        {
            $this->header = $header;
        }

        /**
         * @return string
         */
        public function getText()
        {
            return $this->text;
        }

        /**
         * @param string $text
         */
        public function setText($text)
        {
            $this->text = $text;
        }

        /**
         * @return string
         */
        public function getImage()
        {
            return $this->image;
        }

        /**
         * @param string $image
         */
        public function setImage($image)
        {
            $this->image = $image;
        }

        public function create()
        {
            require_once __DIR__."/../views/about_us/about_us.php";
        }

    }