<?php

    class HeaderClass
    {
        /**
         * @var string
         */
        private $type = "";

        /**
         * @var string
         */
        private $image = "";

        /**
         * @var array
         */
        private $images = [];

        /**
         * @return string
         */
        public function getType()
        {
            return $this->type;
        }

        /**
         * @param string $type
         */
        public function setType($type)
        {
            $this->type = $type;
        }

        /**
         * @return string
         */
        public function getImage()
        {
            return $this->image;
        }

        /**
         * @param string $image
         */
        public function setImage($image)
        {
            $this->image = $image;
        }

        public function getImagesForSlider()
        {
            $directory = "images/slider";
            $images = glob($directory . "/*.jpg");

            foreach($images as $image)
            {
                $this->images[] = $image;
            }
        }

        /**
         * @return array
         */
        public function getImages()
        {
            return $this->images;
        }

        /**
         * @param array $images
         */
        public function setImages($images)
        {
            $this->images = $images;
        }

        public function create()
        {
            $this->getImagesForSlider();
            switch ($this->type)
            {
               case "header":
                   require_once __DIR__."/../views/headers/header.php";
                   break;

               case "headerSlider":
                   require_once __DIR__."/../views/headers/header_slider.php";
                   break;

               default:
                   die("morate upisati tip headera!");

            }
        }

    }
