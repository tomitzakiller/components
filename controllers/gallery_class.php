<?php

    class GalleryClass
    {

        /**
         * @var array
         */
        private $images = [];

        /**
         * @return array
         */
        public function getImages()
        {
            return $this->images;
        }


        public function getImagesFromGalleryFolder()
        {
            $directory = "images/gallery";
            $images = glob($directory . "/*.jpg");

            foreach($images as $image)
            {
                $this->images[] = $image;
            }
        }

        /**
         * @param array $images
         */
        public function setImages($images)
        {
            $this->images = $images;
        }

        public function create ()
        {
            $this->getImagesFromGalleryFolder();
            require_once __DIR__."/../views/gallery/gallery.php";
        }

    }