<?php

DEFINE('DS', DIRECTORY_SEPARATOR);

class InfoNavigation
    {

        /**
         * @var array
         */
        private $icons = [];

        /**
         * @var
         */
        private $contactInfo;

        /**
         * @return mixed
         */
        public function getContactInfo()
        {
            return $this->contactInfo;
        }

        /**
         * @param mixed $contactInfo
         */
        public function setContactInfo($contactInfo)
        {
            $this->contactInfo = $contactInfo;
        }


        /**
         * @return array
         */
        public function getIcons()
        {
            return $this->icons;
        }

        /**
         * @param array $icons
         */
        public function setIcons($icons)
        {
            $this->icons = $icons;
        }


        /**
         * @param $iconName
         * @param $iconUrl
         */
        public function addIcon($iconName, $iconUrl)
        {
            $this->icons[$iconName] = $iconUrl;
        }

        public function create()
        {
            require_once  __DIR__."/../views/info_navigation/info_nav.php";
        }
    }
