<?php

class AdaptationClass
{
    /**
     * @var string
     */
    private $type = "";

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    public function create()
    {
        switch($this->type)
        {
            case "apartment":
                require_once __DIR__."/../views/adaptation/adaptation_apartment.php";
                break;

            case "business":
                require_once __DIR__."/../views/adaptation/adaptation_business.php";
                break;
        }



    }


}