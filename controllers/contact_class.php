<?php

    class ContactClass
    {
        /**
         * @var string
         */
        private $address = "";

        /**
         * @var string
         */
        private $email = "";

        /**
         * @var string
         */
        private $url = "";

        /**
         * @var string
         */
        private $phone = "";

        /**
         * @return string
         */
        private $city = "";

        /**
         * @var bool
         */
        private $visible = true;

        /**
         * @var string
         */
        private $title = "";

        /**
         * @return string
         */
        public function getTitle()
        {
            return $this->title;
        }

        /**
         * @param string $title
         */
        public function setTitle($title)
        {
            $this->title = $title;
        }

        /**
         * @return string
         */
        public function getText()
        {
            return $this->text;
        }

        /**
         * @param string $text
         */
        public function setText($text)
        {
            $this->text = $text;
        }

        /**
         * @var string
         */
        private $text = "";


        /**
         * @return bool
         */
        public function isVisible()
        {
            return $this->visible;
        }

        /**
         * @param bool $visible
         */
        public function setVisible($visible)
        {
            $this->visible = $visible;
        }

        /**
         * @return mixed
         */
        public function getCity()
        {
            return $this->city;
        }

        /**
         * @param mixed $city
         */
        public function setCity($city)
        {
            $this->city = $city;
        }

        public function getAddress()
        {
            return $this->address;
        }

        /**
         * @param string $address
         */
        public function setAddress($address)
        {
            $this->address = $address;
        }

        /**
         * @return string
         */
        public function getEmail()
        {
            return $this->email;
        }

        /**
         * @param string $email
         */
        public function setEmail($email)
        {
            $this->email = $email;
        }

        /**
         * @return string
         */
        public function getUrl()
        {
            return $this->url;
        }

        /**
         * @param string $url
         */
        public function setUrl($url)
        {
            $this->url = $url;
        }

        /**
         * @return string
         */
        public function getPhone()
        {
            return $this->phone;
        }

        /**
         * @param string $phone
         */
        public function setPhone($phone)
        {
            $this->phone = $phone;
        }

        public function create()
        {
            if($this->isVisible())
            {
                require_once  __DIR__."/../views/contact/contact.php";
            }
        }


    }