<?php

    class OfferClass
    {
        /**
         * @var array
         */
        private $offer = [];

        /**
         * @return array
         */
        public function getOffer()
        {
            return $this->offer;
        }

        /**
         * @param array $offer
         */
        public function setOffer($offer)
        {
            $this->offer = $offer;
        }

        public function create()
        {
            require_once __DIR__."/../views/offer/offer.php";
        }
    }