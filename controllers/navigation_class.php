<?php

   class NavigationClass
   {
       /**
        * @var string
        */
       private $logoText = "";

       /**
        * @var array
        */
       private $logoImage =[];
       /**
        * @var array
        */
       private $options = [];

       /**
        * @return array
        */
       public function getLogoImage()
       {
           return $this->logoImage;
       }

       /**
        * @param array $logoImage
        */
       public function setLogoImage($logoImage)
       {
           $this->logoImage = $logoImage;
       }

       /**
        * @return string
        */
       public function getLogoText()
       {
           return $this->logoText;
       }

       /**
        * @param string $logoText
        */
       public function setLogoText($logoText)
       {
           $this->logoText = $logoText;
       }

       /**
        * @return array
        */
       public function getOptions()
       {
           return $this->options;
       }

       /**
        * @param array $options
        */
       public function setOptions($options)
       {
           $this->options = $options;
       }

       public  function create()
       {
          require_once __DIR__."/../views/navs/nav.php";
       }
   }