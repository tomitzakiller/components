<?php

    class FooterClass
    {
        /**
         * @var int
         */
       private $year = 0;

       private $contactInfo;

        /**
         * @return Object
         */
        public function getContactInfo()
        {
            return $this->contactInfo;
        }

        /**
         * @param Object $contactInfo
         */
        public function setContactInfo($contactInfo)
        {
            $this->contactInfo = $contactInfo;
        }

        /**
         * @return int
         */
        public function getYear()
        {
            return $this->year;
        }

        /**
         * @param int $year
         */
        public function setYear($year)
        {
            $this->year = $year;
        }

        /**
         * @return string
         */

        public function create()
        {
            require_once __DIR__."/../views/footer/footer.php";
        }
    }