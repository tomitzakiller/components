$( document ).ready(function() {

    var windowWidth = $(window).width();

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var height = 40;

        if (scroll > height) {

            $(".mainNavDiv").addClass("fixed-top");
        }
        else{
            $(".mainNavDiv").removeClass("fixed-top");
        }
    });

});