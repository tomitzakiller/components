<!DOCTYPE html>
<html>
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <script src="jquery.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <script src="script.js"></script>
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>

    <body>

    <?php

        require_once "controllers/info_navigation_class.php";
        require_once "controllers/navigation_class.php";
        require_once "controllers/header_class.php";
        require_once "controllers/contact_class.php";
        require_once "controllers/footer_class.php";
        require_once "controllers/adaptation_class.php";
        require_once "controllers/offer_class.php";
        require_once "controllers/engagement_class.php";
        require_once "controllers/about_us_class.php";
        require_once "controllers/gallery_class.php";

        $contact = new ContactClass();
        $contact->setEmail("info@mojsajt.com");
        $contact->setAddress("MOJA ULOICA BR. 5");
        $contact->setCity("11000 BEOGRAD");
        $contact->setPhone("060/088 06 06");
        $contact->setText("Bla bla");
        $contact->setTitle("Test test");
        $contact->setUrl("https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11296.463558544663!2d20.1940951!3d44.941312!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a59363f9797f3%3A0x8e4185e772a0d914!2sMedius!5e0!3m2!1ssr!2srs!4v1577914827108!5m2!1ssr!2srs");
        $contact->setVisible(true);


        $infoNav = new InfoNavigation();
        $infoNav->addIcon("<i class=\"fab fa-facebook-square colorBlueFont \"></i>", "https://www.facebook.com/4BrothersReconstruction/");
        $infoNav->setContactInfo($contact);
        $infoNav->create();

        $nav = new NavigationClass();
        $nav->setLogoImage("images/logo.png");
        $nav->setOptions([
            "POČETNA" => "test",
            "O NAMA" => "o-nama",
            "RENOVIRANJE" => "renoviranje",
            "GALERIJA" => "galerija",
            "KONTAKT" => "kontakt"
        ]);
        $nav->create();

        $header = new HeaderClass();
        $header->setType("headerSlider");
        $header->setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1584fZ73b_mn8X9yMGpv083t3CsyPOt261wHsrrLiSVu_DljQuA&s");
        $header->create();

        $adaptation_apartment = new AdaptationClass();
        $adaptation_apartment->setType("apartment");
        $adaptation_apartment->create();

        $offer = new OfferClass();
        $offer->setOffer([
                    'ELEKTRIKA' => '<i class="fas fa-plug"></i>',
                    'GREJANJE I KLIMATIZACIJA' => '<i class="fas fa-temperature-high"></i>',
                    'VODOVOD' => '<i class="fas fa-shower"></i>',
                    'MOLERAJ' => '<i class="fas fa-paint-roller"></i>',
                    'PVC I ALU STOLARIJA' => '<i class="fas fa-door-open"></i>',
                    'GIPSARSKI RADOVI' => '<i class="fas fa-pallet"></i>',
                    'KERAMIKA' => '<i class="fab fa-buffer"></i>',
                    'PODOVI' => '<i class="fas fa-ruler-combined"></i>'
        ]);
        $offer->create();


        $adaptation_business = new AdaptationClass();
        $adaptation_business->setType("business");
        $adaptation_business->create();

        $chat = new EngagementClass();
        $chat->setType("chat");
        $chat->create();

        $aboutUs = new AboutUsClass();
        $aboutUs->setHeader("MOLERAJ");
        $aboutUs->setText("
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            ");
        $aboutUs->setImage("images/before_and_after.png");
        $aboutUs->setOfferData([
            'Elektrika' => [
                'Zamena sukoa, prekidača, sijaličnih grla, sijalica...',
                'Instalacija sukoa, prekidača, rasvete...',
                'Instalacija bele tehnike',
                'Izrada elektro ormara',
                'icon' => '<i class="fas fa-plug"></i>',
            ],
            'Grejanje i klimatizacija' => [
                'Ugradnja klima uredjaja',
                'Ugradnja instalacija i grejnih tela',
                'Ugradnja kotlova i toplotnih pumpi',
                'icon' => '<i class="fas fa-temperature-high"></i>',
            ],
            'Vodovod' => [
                'Izrada nove vodovodne mreže',
                'Instalacija sanitarija',
                'Zamena postojece instalacije',
                'icon' => '<i class="fas fa-shower"></i>',
            ],
            'Moleraj' => [
                'Obrada spaletni',
                'Krečenje',
                'Gletovanje',
                'Fasade',
                'icon' => '<i class="fas fa-paint-roller"></i>',
            ],
            'Keramika' => [
                'Postavljanje pločica',
                'Košuljica',
                'Hidroizolacija',
                'icon' => '<i class="fab fa-buffer"></i>',
            ],
            'Gipsarski radovi' => [
                'Spušteni plafon',
                'Pregradni zidovi',
                'icon' => '<i class="fas fa-pallet"></i>',
            ],
            'Stolarija' => [
                'Izrada i ugradnja PVC i ALU stolarije',
                'icon' => '<i class="fas fa-door-open"></i>',
            ],
            'Podovi' => [
                'Postavljanje laminata',
                'Postavljanje parketa',
                'icon' => '<i class="fas fa-ruler-combined"></i>'
            ],
        ]);
        $aboutUs->create();

        $gallery = new GalleryClass();
        $gallery->create();

        $contact->create();

        $footer = new FooterClass();
        $footer->setYear(2020);
        $footer->setContactInfo($contact);
        $footer->create();

    ?>
    </body>

</html>


